const { deterministicPartitionKey, createHash, TRIVIAL_PARTITION_KEY, MAX_PARTITION_KEY_LENGTH } = require("./dpk");

describe('deterministicPartitionKey', () => {
  it("Returns the literal '0' when given no input", () => {
    expect(deterministicPartitionKey()).toBe('0');
  });

  test('returns TRIVIAL_PARTITION_KEY when given null', () => {
    expect(deterministicPartitionKey(null)).toBe(TRIVIAL_PARTITION_KEY);
  });

  test('returns event.partitionKey when the attribute presents', () => {
    const event = { partitionKey: 'njs' };
    expect(deterministicPartitionKey(event)).toBe(event.partitionKey);
  });

  test('returns a generated partition key when event.partitionKey is not present', () => {
    const event = { mine: 'craft' };
    const data = JSON.stringify(event);
    const expectedPartitionKey = createHash(data);
    expect(deterministicPartitionKey(event)).toBe(expectedPartitionKey);
  });

  test('returns a generated partition key when event.partitionKey exceeds MAX_PARTITION_KEY_LENGTH', () => {
    const longPartitionKey = 'tony_hawk_makes_a_flip_and_then_'.repeat(MAX_PARTITION_KEY_LENGTH + 1);
    const expectedPartitionKey = createHash(longPartitionKey);
    expect(deterministicPartitionKey({ partitionKey: longPartitionKey })).toBe(expectedPartitionKey);
  });
});