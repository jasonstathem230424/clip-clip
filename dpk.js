const crypto = require("crypto");


const TRIVIAL_PARTITION_KEY = "0";
const MAX_PARTITION_KEY_LENGTH = 256;


const createHash = (data) => crypto.createHash("sha3-512").update(data).digest("hex");

const getPartitionKey = (event) => event.partitionKey || createHash(JSON.stringify(event));


const deterministicPartitionKey = (event) => {
  const rawCandidate = event ? getPartitionKey(event) : TRIVIAL_PARTITION_KEY;
  const candidate =  typeof rawCandidate === 'string' ? rawCandidate : JSON.stringify(rawCandidate);

  return candidate.length > MAX_PARTITION_KEY_LENGTH ? createHash(candidate) : candidate;
};


module.exports = {
  createHash,
  deterministicPartitionKey,
  TRIVIAL_PARTITION_KEY,
  MAX_PARTITION_KEY_LENGTH,
};