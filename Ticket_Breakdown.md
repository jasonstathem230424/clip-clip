# Ticket Breakdown
We are a staffing company whose primary purpose is to book Agents at Shifts posted by Facilities on our platform. We're working on a new feature which will generate reports for our client Facilities containing info on how many hours each Agent worked in a given quarter by summing up every Shift they worked. Currently, this is how the process works:

- Data is saved in the database in the Facilities, Agents, and Shifts tables
- A function `getShiftsByFacility` is called with the Facility's id, returning all Shifts worked that quarter, including some metadata about the Agent assigned to each
- A function `generateReport` is then called with the list of Shifts. It converts them into a PDF which can be submitted by the Facility for compliance.

## You've been asked to work on a ticket. It reads:

**Currently, the id of each Agent on the reports we generate is their internal database id. We'd like to add the ability for Facilities to save their own custom ids for each Agent they work with and use that id when generating reports for them.**


Based on the information given, break this ticket down into 2-5 individual tickets to perform. Provide as much detail for each ticket as you can, including acceptance criteria, time/effort estimates, and implementation details. Feel free to make informed guesses about any unknown details - you can't guess "wrong".


You will be graded on the level of detail in each ticket, the clarity of the execution plan within and between tickets, and the intelligibility of your language. You don't need to be a native English speaker, but please proof-read your work.

## Your Breakdown Here

Considering the context, I assume that 
- we have an SQL DB  
- Agents aren't full-time employees, so any agent can work for multiple Facilities, which means we have a many-to-many relationship.


#### t-1: create a new FacilityAgent table
Create a new table with the "facility_agent_id" column with the unique constraint, and "facility_id" and "agent_id" foreign keys. Add tests to ensure all constraints and references are correct.

Acceptance criteria:
The new FacilityAgent table has the "facility_agent_id" column with the unique constraint, and "facility_id" and "agent_id" foreign keys.
Tests cover the new table constraints and references.

time: 2h


#### t-2: add a new "getFacilityAgentId" function
The function takes "agent_id" and "facility_id" arguments. It hecks if there's a match in the FacilityAgent table and returns "facility_agent_id" if so, otherwise "agent_id". Tests should cover the new function for different cases.

Acceptance criteria:
The new function returns a valid result based on arguments and db data.
Tests cover different scenarios.

time: 3h



#### t-3 use "getFacilityAgentId" in "getShiftsByFacility"
**We can skip this ticket if "getShiftsByFacility" doesn't operate with "agent_id".**
Update the "getShiftsByFacility" function to use "getFacilityAgentId" and return "facility_agent_id" as agent ids when applicable. Tests cover different scenarios. 

Acceptance criteria:
"getShiftsByFacility" uses "getFacilityAgentId" instead of a simple return of "agent_id"
Tests cover different scenarios

time: 4h (may depend on the "getShiftsByFacility" complexity)


#### t-4 use "getFacilityAgentId" in "generateReport"
**We can skip this ticket if "generateReport" doesn't operate with "agent_id".**
Update the "generateReport" function to use "getFacilityAgentId" and return "facility_agent_id" as agent ids when applicable. Tests cover different scenarios. 

Acceptance criteria:
"generateReport" uses "getFacilityAgentId" instead of a simple return of "agent_id"
Tests cover different scenarios

time: 4h (may depend on the "generateReport" complexity)


#### t-5 provide CRUD operations that are allowed for the "FacilityAgent" table to Facilities
Based on the system limitations, facilities should be able to CRUD data in the "FacilityAgent" table. Tests cover the new functionality.

Acceptance criteria:
Create an API for facilities to be able to CRUD (might have some limitations) data in the "FacilityAgent" table
Tests cover new functionality

Time: 5h (may depend on the current API complexity and if it's BE only or BE+FE)